# ---------------------------------------------------- #
# File: project2.py
# ---------------------------------------------------- #
# Author(s): Yifan Li, Guanyang Ye with BitBucket handle (frank_li90)
# [you may add confluence or github link]
# ---------------------------------------------------- #
# Plaftorm:    {Windows | Unix (includes Mac OS)}
# Environment: Python           2.7.8
# Libaries:    numpy            1.9.0
#              matplotlib  1.4.0
#                   scipy       0.14.0-Py2.7
#              . . .
#                   sympy       0.7.5
# ---------------------------------------------------- #
# Description:
"""
Generate a triangle with three random vertices on a 256 X 256 pixels plane. 
Rotate the triangle at user-defined angular velocity for user-defined time period. 
The program will also save 100 pictures as the triangle spins. 

"""
# <numpy>
# <matplotlib.pyplot>
# <matplotlib.animation>
# <scipy.ndimage>
# <scipy.misc>
# <glob?
# <random>
# ---------------------------------------------------- #

import numpy as np

import matplotlib.pyplot as plt

import matplotlib.animation as animation

import scipy.ndimage as ndimage

import scipy.misc as misc

from glob import glob

import random

import math

# ------------------------

"""
	Class TriAngle includes the following methods: 
	
	1\ Creat an empty plane (0s) on which the triangle will be placed.
	2\ Generate three random vertices for the triangle.
	3\ Deciding wether a point falls within the triangle
	4\ Construct the triangle using the method from "3" 
	5\ Spin the triangle at user-defined rate for user-defined time. Save 100 snapshots of the triangle as it spins.
	6\ 
	7\ 
	8\ 
	9\ 


""" 

class TriAngle:

	def __init__(self,sigma):

		print 'A new triangle has been constructed!'

		self.tri_im = np.zeros((256,256))

		self.sigma = sigma

	def GenerateVertices(self):

		self.A = (128, int(128+64*random.random()))

		self.B = (64+int(64*random.random()), 64+int(64*random.random()))

		self.C = (128+int(64*random.random()), 64+int(64*random.random()))

		print 'Coordinates of the vertices: \n', 'A: ', self.A, '\n', 'B: ', self.B, '\n', 'C: ', self.C, '\n'

		self.tri_im[self.A[0], self.A[1]] = 1

		self.tri_im[self.B[0], self.B[1]] = 1

		self.tri_im[self.C[0], self.C[1]] = 1

	def IsPointInit(self,point):

		self.PA = (self.A[0]-point[0], self.A[1]-point[1])

		self.PB = (self.B[0]-point[0], self.B[1]-point[1])

		self.PC = (self.C[0]-point[0], self.C[1]-point[1])

		if((np.cross(self.PA,self.PB)>0)&(np.cross(self.PB,self.PC)>0)&(np.cross(self.PC,self.PA)>0)):
			return True
		else:
			return False

	def ConstructTri(self):

		for ii in range(256):

			for jj in range(256):

				if(self.IsPointInit((ii,jj))):

					self.tri_im[ii, jj] = 1.0

		self.tri_im = ndimage.gaussian_filter(self.tri_im, self.sigma)

	# Spin and Save, rate is in 'rad/s'

	def updateFig(self,ii):

		return plt.imshow(misc.imread(self.IMGLIST[ii]), cmap=plt.cm.gray,interpolation='none'), 

	def GetFiles(self, prefix='triangle'):

		self.IMGLIST = glob(prefix+'*.png')

		self.fig = plt.figure(figsize=(20,20))

		self.ani = animation.FuncAnimation(self.fig, self.updateFig,frames=range(len(self.IMGLIST)),interval=5,blit=True )

		plt.show()

	def SpinAndSave(self, rate, time, nimages=100, prefix='triangle'):

		for ii in range(nimages):

			misc.imsave( prefix+'_%02d.png' % (ii), self.tri_im)

			self.tri_im = ndimage.rotate(self.tri_im, float(rate*time/nimages), reshape=False)

	def GetBinaryVertexImage(self, triangle_image):

		self.im = misc.imread(triangle_image)

		self.im_original = self.im/(np.max(self.im))

		misc.imread(triangle_image)

		self.im_processed = ndimage.binary_opening(self.im_original)

		self.im_processed = ndimage.binary_opening(self.im_processed)

		self.im_processed = ndimage.binary_opening(self.im_processed)

		mask = (self.im_original>self.im_processed)

		self.labeled_im, self.counts = ndimage.label(mask)

		plt.figure(figsize = (20,20))

		plt.imshow(self.labeled_im,cmap = plt.cm.gray, interpolation = 'none')

		plt.show()

	def GetHarrisVerticesImage(self, triangle_im, kappa=0.16, windowSize=9, windowSpace=2, sigma=1):

		self.triangle_image = misc.imread(triangle_im)/np.max(misc.imread(triangle_im))

		print self.triangle_image

		self.Ix = ndimage.sobel( self.triangle_image, axis = 0 , mode = 'constant' )

		self.Iy = ndimage.sobel( self.triangle_image, axis = 1 , mode = 'constant')

		self.N = int(math.sqrt(windowSize))

		self.gw = np.zeros((self.N,self.N))

		self.center = (self.N-1)/2

		self.gw[self.center, self.center] = 1 

		self.gw = ndimage.gaussian_filter(self.gw, sigma)

		self.shape = (len(self.triangle_image), len(self.triangle_image))

		self.harrisI = np.zeros(self.shape)

		for ix in range( self.center, self.shape[0]-self.center, windowSpace ):

			for iy in range( self.center, self.shape[1]-self.center, windowSpace ):

				self.Ix_window = self.Ix[( ix-self.center):(ix-self.center + self.N) , (iy-self.center):(iy-self.center+self.N) ]

				self.Iy_window = self.Iy[( ix-self.center):(ix-self.center + self.N) , (iy-self.center):(iy-self.center+self.N) ]

				self.Ix_sq = np.multiply(self.Ix_window, self.Ix_window)

				self.Iy_sq = np.multiply(self.Iy_window, self.Iy_window)

				self.IxIy = np.multiply(self.Ix_window, self.Iy_window)

				self.Ix_sq_avg = np.sum(np.multiply(self.Ix_sq, self.gw))

				self.Iy_sq_avg = np.sum(np.multiply(self.Iy_sq, self.gw))

				self.IxIy_avg = np.sum(np.multiply(self.IxIy, self.gw))

				self.M = np.matrix( [ [self.Ix_sq_avg, self.IxIy_avg] ,[self.IxIy_avg, self.Iy_sq_avg] ] )

				self.harrisI[ix, iy] = np.linalg.det(self.M) - kappa * pow( np.trace(self.M) , 2.0)

				# if(self.harrisI[ix, iy]>10):

				# 	self.harrisI[ix, iy] = 1

		plt.figure(figsize=(20,20))

		plt.imshow(self.harrisI, cmap = plt.cm.gray, interpolation='none')

		plt.show()

			 # this is not an image, just the image matrix. you may want to further manipulate it to get what you want.

# ------------------------
# END-OF-Class:->myClass<-


tri1 = TriAngle(0.0)

tri1.GenerateVertices()

tri1.ConstructTri()

tri1.SpinAndSave(10,30)

tri1.GetFiles()

tri1.GetBinaryVertexImage(tri1.IMGLIST[0])

tri1.GetHarrisVerticesImage(tri1.IMGLIST[0])

# ---------------------------------------------------- #
# END-OF-File:->your_file_name.py<-
# ---------------------------------------------------- #
