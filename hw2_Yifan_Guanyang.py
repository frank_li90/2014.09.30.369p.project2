# ---------------------------------------------------- #
# File: hw2_Yifan_Guanyang.py
# ---------------------------------------------------- #
# Author(s): Yifan Li, Guanyang Ye with BitBucket handle (frank_li90, PaulYe123)
# [you may add confluence or github link]
# ---------------------------------------------------- #
# Plaftorm:    {Windows | Mac OSX Mavericks}
# Environment: Python           2.7.8
# Libaries:    numpy            1.9.0
#              matplotlib       1.4.0
#              scipy            0.14.0-Py27
#              cv2              2.4.9-Py27
# ---------------------------------------------------- #
# Description:
"""
Generate a triangle with three random vertices on a 256 X 256 pixels plane. 
Rotate the triangle at user-defined angular velocity for user-defined time period. 
The program will also save 100 pictures as the triangle spins. 

"""
# <numpy>
# <matplotlib.pyplot>
# <matplotlib.animation>
# <scipy.ndimage>
# <scipy.misc>
# <glob>
# <random>
# <math>
# <cv2>
# ---------------------------------------------------- #

import numpy as np

import matplotlib.pyplot as plt

import matplotlib.animation as animation

import scipy.ndimage as ndimage

import scipy.misc as misc

from glob import glob

import random

import math

import cv2

# ------------------------

"""
	Class TriAngle includes the following methods: 
	
	1\ Creat an empty plane (0s) on which the triangle will be placed.
	2\ Generate three random vertices for the triangle.
	3\ Deciding wether a point falls within the triangle
	4\ Construct the triangle using the method from "3" 
	5\ Spin the triangle at user-defined rate for user-defined time. Save 100 snapshots of the triangle as it spins.
	6\ Make animation of rotating triangle
	7\ Detect vertices using ndimage binary functions
	8\ Detect vertices using open cv (cv2) harris function 
	9\ Track vertices when the triangle rotates


""" 

class TriAngle:
	def __init__(self): 
		print 'A new triangle has been constructed!'
		# Create a 256 x 256 array with zeors
		self.tri_im = np.zeros((256,256))


	# Generate three vertices of a triangle
	# Point A falls on y-axis; B in third quadrant ; C in fourth quadrant
	# Assign each vertice value of 1 in the array just created
	def GenerateVertices(self):
		self.A = (128, int(128+64*random.random()))
		self.B = (64+int(64*random.random()), 64+int(64*random.random()))
		self.C = (128+int(64*random.random()), 64+int(64*random.random()))
		print 'Coordinates of the vertices: \n', 'A: ', self.A, '\n', 'B: ', self.B, '\n', 'C: ', self.C, '\n'
		self.tri_im[self.A[0], self.A[1]] = 1
		self.tri_im[self.B[0], self.B[1]] = 1
		self.tri_im[self.C[0], self.C[1]] = 1


	# Determine if a point P is inside the triangle by cross product
	# If the cross products of between each line formed by P and A, B, C are all postive
	# Then P is inside the triangle. Vice versa.
	def IsPointInit(self,point):
		self.PA = (self.A[0]-point[0], self.A[1]-point[1])
		self.PB = (self.B[0]-point[0], self.B[1]-point[1])
		self.PC = (self.C[0]-point[0], self.C[1]-point[1])
		if((np.cross(self.PA,self.PB)>0)&(np.cross(self.PB,self.PC)>0)&(np.cross(self.PC,self.PA)>0)):
			return True
		else:
			return False

	# Assign value 1 to each point that is inside the triangle in the array
	# Blur the image (still in array form) with gaussian filter by a gaussian smoothness of sigma
	def ConstructTri(self, sigma):
		for ii in range(256):
			for jj in range(256):
				if(self.IsPointInit((ii,jj))):
					self.tri_im[ii, jj] = 1.0
		self.tri_im = ndimage.gaussian_filter(self.tri_im, sigma)



	# Rotate the triangle in the array and save each one of the rotated triangle
	def SpinAndSave(self, rate, time, nimages=100, prefix='triangle'):
		for ii in range(nimages):
			misc.imsave( prefix+'_%02d.png' % (ii), self.tri_im)
			self.tri_im = ndimage.rotate(self.tri_im, rate*time/nimages, reshape=False)


	# Update figure for GetImageFiles method in this class
	def UpdateFig(self, ii):
		return plt.imshow(misc.imread(self.IMGLIST[ii]),  interpolation='none'),  


	# Retrieve all saved rotated triangle images from the folder then show them in an animation
	def GetImageFiles(self, prefix):
		# Get all files that have the indicated prefix
		self.IMGLIST = glob(prefix+'*.png')
		# Format figure that is about to show
		self.fig = plt.figure(figsize=(10,10))
		# Make animation from the images
		self.ani = animation.FuncAnimation(self.fig, self.UpdateFig,frames=range(len(self.IMGLIST)),interval=50,blit=False )
		plt.show()


	# Use built in function in ndimage to detect vertices (corners)
	def GetBinaryVertexImage(self, triangle_image):
		# Transfer image file back to an array
		self.im = misc.imread(triangle_image)
		# Normalize the array so that each value in each pixel is less than 1
		self.im_normalized = self.im/(np.max(self.im))
		# Open gaps between pixels using binary_opening method
		self.im_processed = ndimage.binary_opening(self.im_normalized)
		# Create a mask; serve as a way to filter out all points but vertices points 
		mask = (self.im_normalized>self.im_processed)
		# Dilate pixels to make the vertices more visible
		self.dilated_im = ndimage.binary_dilation(mask)
		plt.figure(figsize = (10,10))
		plt.imshow(self.dilated_im, interpolation = 'none') 
		plt.show()

	# Use Open CV library to implement Harris corner detection
	# Detailed explanation can be found at http://docs.opencv.org/modules/imgproc/doc/feature_detection.html#cornerharris
	def GetHarrisVertexImage(self, triangle_im, piture_number, switch = True):
		self.im = misc.imread(triangle_im) 
		# Method cornerHarris returns an array dst that is the same dimensions as self.im, but with filtered points in each pixel 
		self.dst = cv2.cornerHarris(self.im,2,3,0.05)
		# Iterations to check if each pixel exceeds certain threshold, in this case, 5*10^-4
		# If the value in a pixel does, which means it is part of a corner, then assign 1 to it
		# Otherwise, assign 0
		for i in range(self.dst.shape[0]):
			for j in range(self.dst.shape[1]):
				if self.dst[i, j] > 5*10e-4:
					self.dst[i, j] = 1
				else: 
					self.dst[i, j] = 0
		# Determine if the user wants to show the corner detection result or simply save it for future use
		if switch == True:
			plt.imshow(self.dst)
			plt.show()
		else:
			prefix = 'FeatureTracking'
			misc.imsave(prefix + '_%02d.png' % (piture_number), self.dst)

	# Track vertices when the triangle is rotation
	def FeatureTracking(self):
		# Detect corners of each triangle using Harris corner dection and save it
		# Retrieve all triangles that have been inspected, then make an animation from them
		for i in range(len(self.IMGLIST)):
			self.GetHarrisVertexImage(self.IMGLIST[i], i, False)
		self.GetImageFiles(prefix = 'FeatureTracking')

# ------------------------
# END-OF-Class:->TriAngle<-


tri1 = TriAngle()

tri1.GenerateVertices()

tri1.ConstructTri(0)

tri1.SpinAndSave(10,30)

tri1.GetImageFiles(prefix = 'triangle')

tri1.GetBinaryVertexImage(tri1.IMGLIST[0])

tri1.GetHarrisVertexImage(tri1.IMGLIST[0], True)

tri1.FeatureTracking()

# Note: Animations and Images shown in turn are:
# 1) Rotating triangle
# 2) Binary vertex detection 
# 3) Harris vertex detection
# 4) Corner tracking


# ---------------------------------------------------- #
# END-OF-File:->hw2_Yifan_Guanyang.py<-
# ---------------------------------------------------- #
